﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace di.services.Controllers
{
    public class GuessGameController : Controller
    {
        //
        // GET: /GuessGame/

        public string RandomNumber(int? from = 1, int? to = 10)
        {
            //picka random, write it out
            return new Random().Next(from.Value, to.Value).ToString();
        }

    }
}
